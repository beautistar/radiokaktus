//
//  InviteViewController.swift
//  RadioKaktus
//
//  Created by Developer on 8/11/19.
//  Copyright © 2019 Radio Kaktus. All rights reserved.
//

import UIKit
import MessageUI


class InviteViewController: UIViewController, MFMailComposeViewControllerDelegate {

    @IBOutlet weak var tfemail: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        initView()
    }
    
    func initView() {
        
        self.setNavigationBarItem()
    }
    
    
    // MARK: - Button Actions
    @IBAction func onPressSend(_ sender: Any) {
        
        if MFMailComposeViewController.canSendMail() {
            
            if tfemail.text!.count == 0 {
                
                self.showAlertDialog(title: "Radio Kaktus", message: "Please input Email address", positive: "OK", negative: nil)
                return
            } else if !isValidEmail(tfemail.text!) {
                self.showAlertDialog(title: "Radio Kaktus", message: "Please input valid email address", positive: "OK", negative: nil)
                return
            } else {
                sendEmail()
            }
        } else {
            self.showAlertDialog(title: "Radio Kaktus", message: "Mail services are not available", positive: "OK", negative: nil)
            return
        }
    }
    
    
    
    @IBAction func onPressSocial(_ sender: Any) {

        let activityViewController = UIActivityViewController(activityItems: ["https://apps.apple.com/us/app/radio-kaktus/id1235375590"], applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
        
        // exclude some activity types from the list (optional)
        activityViewController.excludedActivityTypes = [ UIActivity.ActivityType.airDrop, UIActivity.ActivityType.postToFacebook, UIActivity.ActivityType.postToVimeo, .postToTwitter ]
        
        // present the view controller
        self.present(activityViewController, animated: true, completion: nil)        
    }
    
    // MARK: - MailComposer
    
    func sendEmail() {
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setSubject("Welcome Radio Kaktus")
            mail.setToRecipients([tfemail.text!])
            mail.setMessageBody("<p>https://apps.apple.com/us/app/radio-kaktus/id1235375590</p>", isHTML: true)
            
            present(mail, animated: true)
        } else {
            // show failure alert
        }
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true)
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }

}

extension InviteViewController {
    
    func showAlertDialog(title: String!, message: String!, positive: String?, negative: String?) {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        if (positive != nil) {
            
            alert.addAction(UIAlertAction(title: positive, style: .default, handler: nil))
        }
        
        if (negative != nil) {
            
            alert.addAction(UIAlertAction(title: negative, style: .default, handler: nil))
        }
        
        self.present(alert, animated: true, completion: nil)
        
    }
    
    private func isValidEmail(_ email: String) -> Bool {
        
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: email)
    }
}
