//
//  RSSFeedViewController.swift
//  RadioKaktus
//
//  Created by Developer on 8/11/19.
//  Copyright © 2019 Radio Kaktus. All rights reserved.
//

import UIKit
import SDWebImage


class RSSFeedViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, XMLParserDelegate {
    
    var myFeed : NSArray = []
    var feedImages :  [AnyObject] = []
    var url: URL!
    
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.

        self.setNavigationBarItem()
        
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 70

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if from != R.FROM_DETAIL {
            loadData()
        }
    }
    
    func loadData() {
        
        switch selectedNews {
        case .Serbia:
            self.title = "News-Serbia"
            url = URL(string: "http://www.blic.rs/rss/Vesti")!
        case .Croatia:
            self.title = "News-Croatia"
            url = URL(string: "https://www.index.hr/rss/vijesti")!
            //url = URL(string: "https://m.vecernji.hr/feeds/latest")!
            //url = URL(string: "https://www.24sata.hr/feeds/najnovije.xml")!
        case .Bosnia:
            self.title = "News-Bosnia"
            url = URL(string: "https://www.klix.ba/rss/vijesti")!
            //url = URL(string: "https://www.klix.ba/rss/vijesti/bih")!            
        }
        
        loadRss(url);
    }
    
    func loadRss(_ data: URL) {
        
        // XmlParserManager instance/object/variable.
        let myParser : XmlParserManager = XmlParserManager().initWithURL(data) as! XmlParserManager
        
        // Put feed in array.
        myFeed = myParser.feeds
        feedImages = myParser.img
        tableView.reloadData()
    }
    
    // MARK: - TableView
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return myFeed.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "FeedCell") as! FeedCell
        
        cell.imvThumbnail.sd_setImage(with: URL(string: feedImages[indexPath.row] as! String), placeholderImage: UIImage(named: "radio_graph_inactive"))
        cell.lblTitle?.text = (myFeed.object(at: indexPath.row) as AnyObject).object(forKey: "title") as? String
        cell.lblContent?.text = (myFeed.object(at: indexPath.row) as AnyObject).object(forKey: "pubDate") as? String
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let img = feedImages[indexPath.row]    
        let feed = myFeed.object(at: indexPath.row) as AnyObject
        let feedDetailVC = self.storyboard?.instantiateViewController(withIdentifier: "FeedDetailViewController") as! FeedDetailViewController
        feedDetailVC.feed = feed
        feedDetailVC.feedImage = img
        self.present(feedDetailVC, animated: true, completion: nil)
    }
}
