//
//  FeedDetailViewController.swift
//  RadioKaktus
//
//  Created by Developer on 8/11/19.
//  Copyright © 2019 Radio Kaktus. All rights reserved.
//

import UIKit
import SDWebImage

class FeedDetailViewController: UIViewController {

    @IBOutlet weak var imvThumbnail: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    
    var feed: AnyObject?
    var feedImage: AnyObject?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        initView()
    }
    
    func initView() {
        
        from = R.FROM_DETAIL
        imvThumbnail.sd_setImage(with: URL(string: feedImage as! String), placeholderImage: UIImage(named: "radio_graph_inactive"))
        lblTitle?.text = (feed?.object(forKey: "title")) as? String
        var descriptionString: String = ""
        descriptionString = feed?.object(forKey: "description") as! String
        if selectedNews == .Croatia {
            if descriptionString.contains("> ") {
                descriptionString = descriptionString.components(separatedBy: "> ")[1]
            }
        }
        lblDescription?.text = descriptionString
        lblDate?.text = (feed?.object(forKey: "pubDate")) as? String
        
    }
    
    @IBAction func doPressClose(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
    }
}
