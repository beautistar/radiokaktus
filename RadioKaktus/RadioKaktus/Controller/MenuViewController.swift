//
//  MenuViewController.swift
//  RadioKaktus
//
//  Created by dragon on 2019/8/8.
//  Copyright © 2019 Radio Kaktus. All rights reserved.
//

import UIKit

enum LeftMenu: Int {
    case main = 0
    case invite
    case news_serbia
    case news_croatia
    case news_bosnia
    case facebook
    case privacy
}

protocol LeftMenuProtocol : class {
    func changeViewController(_ menu: LeftMenu)
}

class MenuViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, LeftMenuProtocol {
    
    var menuNames = [R.menu1, R.menu2, R.menu3, R.menu4, R.menu5, R.menu6, R.menu7]
    @IBOutlet weak var tblMenu: UITableView!
    
    var mainViewController:UIViewController!
    var inviteViewController: UIViewController!
    var feedViewController: UIViewController!
    var fbViewController: UIViewController!
    var privacyController: UIViewController!
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        initView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.```
    }
    
    func initView() {
        
        tblMenu.tableFooterView = UIView()
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)

        let mainTabViewController = storyboard.instantiateViewController(withIdentifier: "RadioViewController") as! RadioViewController
        self.mainViewController = UINavigationController(rootViewController: mainTabViewController)
        
        let feedVC = storyboard.instantiateViewController(withIdentifier: "RSSFeedViewController") as! RSSFeedViewController
        self.feedViewController = UINavigationController(rootViewController: feedVC)
        
        let inviteVC = storyboard.instantiateViewController(withIdentifier: "InviteViewController") as! InviteViewController
        self.inviteViewController = UINavigationController(rootViewController: inviteVC)
        
        let fbVC = storyboard.instantiateViewController(withIdentifier: "FBViewController") as! FBViewController
        self.fbViewController = UINavigationController(rootViewController: fbVC)
        
        let privacyVC = storyboard.instantiateViewController(withIdentifier: "PrivacyViewController") as! PrivacyViewController
        self.privacyController = UINavigationController(rootViewController: privacyVC)
        
    }
    
    /// LeftMenu Protocol
    
    func changeViewController(_ menu: LeftMenu) {
        
        switch menu {
            
        case .main:
            self.slideMenuController()?.changeMainViewController(self.mainViewController, close: true)
            
        case .invite:
            self.slideMenuController()?.changeMainViewController(self.inviteViewController, close: true)
            
        case .news_serbia:
            selectedNews = .Serbia
            from = R.FROM_MAIN
            self.slideMenuController()?.changeMainViewController(self.feedViewController, close: true)
            
        case .news_croatia:
            selectedNews = .Croatia
            from = R.FROM_MAIN
            self.slideMenuController()?.changeMainViewController(self.feedViewController, close: true)
            
        case .news_bosnia:
            selectedNews = .Bosnia
            from = R.FROM_MAIN
            self.slideMenuController()?.changeMainViewController(self.feedViewController, close: true)
            
        case .facebook:
            self.slideMenuController()?.changeMainViewController(self.fbViewController, close: true)
            
        case .privacy:
            self.slideMenuController()?.changeMainViewController(self.privacyController, close: true)
        
        }
    }
    
    // TableView datasource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuNames.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = UITableViewCell(style: UITableViewCell.CellStyle.default, reuseIdentifier: "MenuCell")
        cell.textLabel?.text = menuNames[indexPath.row]
        cell.backgroundColor = .clear
        cell.textLabel?.textColor = .white
        cell.selectionStyle = .none
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let menu = LeftMenu(rawValue: indexPath.row) {
            self.changeViewController(menu)
        }
    }
}

extension UITableViewCell {
    
    func setSelected(_ selected: Bool, animated: Bool) {
        if selected {
            contentView.backgroundColor = UIColor.green
        } else {
            contentView.backgroundColor = UIColor.blue
        }
    }
}
