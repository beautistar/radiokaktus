//
//  RadioViewController.swift
//  RadioKaktus
//
//  Created by dragon on 2019/8/9.
//  Copyright © 2019 Radio Kaktus. All rights reserved.
//

import UIKit
//import AVFoundation
import MediaPlayer

class RadioViewController: UIViewController, RadioDelegate, AVAudioPlayerDelegate {
    
    @IBOutlet weak var channel1View: UIView!
    @IBOutlet weak var channel1GraphImageView: UIImageView!
    @IBOutlet weak var channel1StatusImageView: UIImageView!
    
    @IBOutlet weak var channel2View: UIView!
    @IBOutlet weak var channel2GraphImageView: UIImageView!
    @IBOutlet weak var channel2StatusImageView: UIImageView!
    
    @IBOutlet weak var channel3View: UIView!
    @IBOutlet weak var channel3GraphImageView: UIImageView!
    @IBOutlet weak var channel3StatusImageView: UIImageView!
    
    @IBOutlet weak var songNameView: UIView!
    @IBOutlet weak var songNameLabel: UILabel!
    @IBOutlet weak var play_pauseButton: UIButton!
    
//    var player : AVPlayer!
//    var playerItem : AVPlayerItem!
   
    var radio: Radio!
    var selectedChannel = 1
    var status = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setNavigationBarItem()
        
        changeStatus()
        radio = Radio.init(userAgent: "RadioKaktus")
        songNameView.isHidden = true
    }
    
    func changeStatus() {
        
        channel1View.backgroundColor = .lightGray
        channel1View.backgroundColor = .lightGray
        channel1View.backgroundColor = .lightGray
        
        channel1GraphImageView.image = UIImage(named: "radio_graph_inactive")
        channel2GraphImageView.image = UIImage(named: "radio_graph_inactive")
        channel3GraphImageView.image = UIImage(named: "radio_graph_inactive")
        
        channel1StatusImageView.image = UIImage(named: "stop_icon")
        channel2StatusImageView.image = UIImage(named: "stop_icon")
        channel3StatusImageView.image = UIImage(named: "stop_icon")
        
        switch selectedChannel {
        case 1:
            channel1View.backgroundColor = UIColor(white: 1, alpha: 1)
            channel1GraphImageView.image = UIImage(named: "radio_graph")
            if status {
                channel1StatusImageView.image = UIImage(named: "pause_icon")
            } else {
                channel1StatusImageView.image = UIImage(named: "play_icon")
            }
        case 2:
            channel1View.backgroundColor = UIColor(white: 1, alpha: 1)
            channel2GraphImageView.image = UIImage(named: "radio_graph")
            if status {
                channel2StatusImageView.image = UIImage(named: "pause_icon")
            } else {
                channel2StatusImageView.image = UIImage(named: "play_icon")
            }
        case 3:
            channel1View.backgroundColor = UIColor(white: 1, alpha: 1)
            channel3GraphImageView.image = UIImage(named: "radio_graph")
            if status {
                channel3StatusImageView.image = UIImage(named: "pause_icon")
            } else {
                channel3StatusImageView.image = UIImage(named: "play_icon")
            }
        default:
            channel1View.backgroundColor = UIColor(white: 1, alpha: 1)
            channel1GraphImageView.image = UIImage(named: "radio_graph")
            if status {
                channel1StatusImageView.image = UIImage(named: "pause_icon")
            } else {
                channel1StatusImageView.image = UIImage(named: "play_icon")
            }
        }
        
    }
    
    func playSound() {
        if !status {
            return
        }
        radio.pause()
        switch selectedChannel {
        case 1:
            radio.connect(R.channel1, with: self, withGain: (1.0))
        case 2:
            radio.connect(R.channel2, with: self, withGain: (1.0))
        case 3:
            radio.connect(R.channel3, with: self, withGain: (1.0))
        default:
            radio.connect(R.channel1, with: self, withGain: (1.0))
        }
        
    }
    
    func metaTitleUpdated(_ title: String!) {
        let tempTitle = String(title.replacingOccurrences(of: ";", with: ""))
        let realTitle = String(tempTitle.split(separator: "'")[1])
        songNameLabel.text = realTitle
        songNameView.isHidden = false
    }
    
    @IBAction func channel1ButtonTapped(_ sender: Any) {
        if selectedChannel == 1 {
            return
        }
        selectedChannel = 1
        playSound()
        changeStatus()
    }
    
    @IBAction func channel2ButtonTapped(_ sender: Any) {
        if selectedChannel == 2 {
            return
        }
        selectedChannel = 2
        playSound()
        changeStatus()
    }
    
    @IBAction func channel3ButtonTapped(_ sender: Any) {
        if selectedChannel == 3 {
            return
        }
        selectedChannel = 3
        playSound()
        changeStatus()
    }
    
    @IBAction func play_pauseButtonTapped(_ sender: Any) {
        
        if status {
            status = false
            radio.pause()
            changeStatus()
            songNameView.isHidden = true
            play_pauseButton.setImage(UIImage(named: "play_btn"), for: .normal)
            
        } else {
            status = true
            playSound()
            changeStatus()
            play_pauseButton.setImage(UIImage(named: "pause_btn"), for: .normal)
        }
    }
    
    
}
