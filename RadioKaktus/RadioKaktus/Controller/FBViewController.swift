//
//  FBViewController.swift
//  RadioKaktus
//
//  Created by Developer on 8/11/19.
//  Copyright © 2019 Radio Kaktus. All rights reserved.
//

import UIKit
import WebKit

class FBViewController: UIViewController {

    @IBOutlet weak var webView: WKWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        initView()
    }
    
    func initView() {
        
        self.setNavigationBarItem()
        
        let myURL = URL(string:"https://www.facebook.com/radiokaktus1/")
        let myRequest = URLRequest(url: myURL!)
        webView.load(myRequest)
    }


}
