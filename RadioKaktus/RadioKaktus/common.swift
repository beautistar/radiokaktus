//
//  common.swift
//  RadioKaktus
//
//  Created by dragon on 2019/8/8.
//  Copyright © 2019 Radio Kaktus. All rights reserved.
//

import Foundation

enum NewsType: Int {
    case Serbia = 0
    case Croatia
    case Bosnia
}

var selectedNews = NewsType.Serbia
var from: Int = 0



struct R {
    
    static let name = "Radio Kaktus"
    
//    static let channel1 = "http://50.7.71.219:7558"
//    static let channel2 = "http://192.240.102.133:11387"
//    static let channel3 = "http://192.240.102.198:14746"
    
    static let channel1 = "http://usa10.fastcast4u.com:5470/stream"
    static let channel2 = "http://usa10.fastcast4u.com:1680/stream"
    static let channel3 = "http://usa10.fastcast4u.com:5730/stream"
    static let menu1 = "Radio"
    static let menu2 = "Invite Friend"
    static let menu3 = "News-Serbia"
    static let menu4 = "News-Croatia"
    static let menu5 = "News-Bosnia"
    static let menu6 = "Facebook"
    static let menu7 = "Privacy Policy"
    
    static let FROM_DETAIL = 1
    static let FROM_MAIN   = 0
}
